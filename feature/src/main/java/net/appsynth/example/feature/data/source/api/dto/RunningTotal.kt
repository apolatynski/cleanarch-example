package net.appsynth.example.feature.data.source.api.dto

import com.google.gson.annotations.SerializedName

data class RunningTotal(
    @SerializedName("daily_average") val dailyAverageSeconds: Int?,
    @SerializedName("human_readable_daily_average") val dailyAverageDisplay: String?,
    @SerializedName("human_readable_total") val totalDisplay: String?,
    @SerializedName("languages") val languages: List<Language>?
)