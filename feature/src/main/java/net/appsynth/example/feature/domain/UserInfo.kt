package net.appsynth.example.feature.domain

data class UserInfo(
    val id: String,
    val displayName: String,
    val fullName: String,
    val email: String?,
    val website: String?,
    val isHireable: Boolean
)