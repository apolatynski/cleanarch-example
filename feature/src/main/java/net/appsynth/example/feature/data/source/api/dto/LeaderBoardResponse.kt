package net.appsynth.example.feature.data.source.api.dto

import com.google.gson.annotations.SerializedName

data class LeaderBoardResponse(
    @SerializedName("data") val data: List<Leader>?
)