package net.appsynth.example.feature.domain

data class Leader(
    val rank: Int,
    val languageStats: LanguageStats,
    val userInfo: UserInfo
)