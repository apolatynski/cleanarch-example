package net.appsynth.example.feature.domain.repository

import net.appsynth.example.feature.domain.usecase.LoadLeaderBoard

interface LeaderBoardRepository {

    suspend fun leaderBoard(): LoadLeaderBoard.Result
}