package net.appsynth.example.feature.data.mapper

import net.appsynth.example.feature.data.source.api.dto.User
import net.appsynth.example.feature.domain.UserInfo

class UserInfoMapper {

    fun map(user: User?): UserInfo? =
        user?.let {
            val id = it.id ?: return null

            if (it.displayName.isNullOrEmpty() && it.fullName.isNullOrEmpty()) return null
            val displayName = it.displayName ?: it.fullName
            val fullName = it.fullName ?: ""

            val isEmailPublic = it.isEmailPublic ?: true
            val email = if (isEmailPublic && it.email.isNullOrEmpty()) it.email else null

            val website = it.website

            val isHireable = it.isHireable ?: false

            UserInfo(id, displayName!!, fullName, email, website, isHireable)
        }
}