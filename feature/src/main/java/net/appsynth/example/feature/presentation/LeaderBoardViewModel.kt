package net.appsynth.example.feature.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.appsynth.example.feature.domain.LanguageStats
import net.appsynth.example.feature.domain.usecase.LoadLeaderBoard
import net.appsynth.example.feature.domain.usecase.SimpleUseCase

class LeaderBoardViewModel(
    private val loadLeaderBoard: SimpleUseCase<LoadLeaderBoard.Result>
) : ViewModel() {

    private val internalLeaderBoard = MutableLiveData<List<LeaderItem>>()
    val leaderBoard: LiveData<List<LeaderItem>> = internalLeaderBoard
    
    private val internalError = MutableLiveData<String?>()
    val error: LiveData<String?> = internalError

    fun refresh() {
        viewModelScope.launch(Dispatchers.IO) {
            when(val data = loadLeaderBoard()) {
                is LoadLeaderBoard.Result.Success -> {
                    val items = data.leaderBoard.map {
                        LeaderItem(
                            it.rank,
                            formatDailyAverageTime(it.languageStats.dailyAverageTime),
                            it.userInfo.displayName,
                            formatTopLanguage(it.languageStats.stats)
                        )
                    }
                    internalLeaderBoard.postValue(items)
                }
                is LoadLeaderBoard.Result.NetworkProblem -> internalError.postValue("Looks like there are some problems with internet connection")
                is LoadLeaderBoard.Result.ServerProblem -> internalError.postValue("Looks like our servers are having some problems")
                is LoadLeaderBoard.Result.Error -> internalError.postValue("Sorry. We encountered some technical problems")
                is LoadLeaderBoard.Result.Unauthorized -> {
                    // TODO: request navigation to login page
                }
            }
        }
    }

    private fun formatDailyAverageTime(averageSeconds: Int): String {
        val hours = averageSeconds / 3600
        val minutes = (averageSeconds - hours * 3600) / 60
        val seconds = averageSeconds % 60

        return when {
            hours > 0 -> "$hours hours, $minutes minutes and $seconds seconds"
            minutes > 0 -> "$minutes minutes and $seconds seconds"
            else -> "$seconds seconds"
        }
    }

    private fun formatTopLanguage(languageStats: Map<String, LanguageStats.Usage>): String {
        val top = languageStats.entries.maxBy { entry -> entry.value.totalSeconds }

        return if (top != null) {
            "${top.key} (${top.value.percentage}%)"
        } else {
            "No information about language usage"
        }
    }
}