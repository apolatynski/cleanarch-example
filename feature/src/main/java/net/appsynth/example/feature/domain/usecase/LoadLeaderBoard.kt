package net.appsynth.example.feature.domain.usecase

import net.appsynth.example.feature.domain.Leader
import net.appsynth.example.feature.domain.repository.LeaderBoardRepository

class LoadLeaderBoard(
    private val leaderBoardRepository: LeaderBoardRepository
) : SimpleUseCase<LoadLeaderBoard.Result> {

    override suspend fun invoke(): Result =
        try {
            leaderBoardRepository.leaderBoard()
        } catch (throwable: Throwable) {
            Result.Error(throwable)
        }

    sealed class Result {
        data class Success(val leaderBoard: List<Leader>) : Result()
        object Unauthorized : Result()
        object ServerProblem : Result()
        object NetworkProblem : Result()
        data class Error(val throwable: Throwable) : Result()
    }
}
