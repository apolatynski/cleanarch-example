package net.appsynth.example.feature.data.repository

import net.appsynth.example.cleanarch.data.source.api.WakaTimeApi
import net.appsynth.example.feature.data.mapper.LeaderBoardMapper
import net.appsynth.example.feature.domain.repository.LeaderBoardRepository
import net.appsynth.example.feature.domain.usecase.LoadLeaderBoard
import retrofit2.HttpException
import timber.log.Timber
import java.net.SocketTimeoutException

class LeaderBoardRepositoryImpl(
    private val wakaTimeApi: WakaTimeApi,
    private val leaderBoardMapper: LeaderBoardMapper
) : LeaderBoardRepository {

    override suspend fun leaderBoard(): LoadLeaderBoard.Result {
        val response = try {
            wakaTimeApi.leaderBoard()
        } catch (e: HttpException) {
            return when (e.code()) {
                401 -> LoadLeaderBoard.Result.Unauthorized
                else -> {
                    Timber.e(e, "Invalid HTTP response")
                    LoadLeaderBoard.Result.ServerProblem
                }
            }
        } catch (e: SocketTimeoutException) {
            return LoadLeaderBoard.Result.NetworkProblem
        }

        return response.data?.let {
            LoadLeaderBoard.Result.Success(it.mapNotNull(leaderBoardMapper::map))
        } ?: LoadLeaderBoard.Result.ServerProblem
    }
}
