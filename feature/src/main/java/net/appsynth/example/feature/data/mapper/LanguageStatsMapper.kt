package net.appsynth.example.feature.data.mapper

import net.appsynth.example.feature.data.source.api.dto.RunningTotal
import net.appsynth.example.feature.domain.LanguageStats

class LanguageStatsMapper {

    fun map(runningTotal: RunningTotal?): LanguageStats =
        runningTotal?.let {
            LanguageStats(
                runningTotal.dailyAverageSeconds ?: 0,
                runningTotal.languages?.mapNotNull {
                    if (it.name.isNullOrEmpty()) return@mapNotNull null
                    if (it.totalSeconds == null || it.totalSeconds == 0f) return@mapNotNull null
                    Pair(it.name, it.totalSeconds.toInt())
                } ?: emptyList()
            )
        } ?: LanguageStats()
}