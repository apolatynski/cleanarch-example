package net.appsynth.example.feature.data.source.api.dto

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("id") val id: String?,
    @SerializedName("display_name") val displayName: String?,
    @SerializedName("email") val email: String?,
    @SerializedName("full_name") val fullName: String?,
    @SerializedName("human_readable_website") val humanReadableWebsite: String?,
    @SerializedName("is_email_public") val isEmailPublic: Boolean?,
    @SerializedName("is_hireable") val isHireable: Boolean?,
    @SerializedName("location") val location: String?,
    @SerializedName("photo") val photoUrl: String?,
    @SerializedName("photo_public") val isPhotoPublic: Boolean?,
    @SerializedName("username") val username: String?,
    @SerializedName("website") val website: String?
)