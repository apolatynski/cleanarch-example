package net.appsynth.example.feature.data.mapper

import net.appsynth.example.feature.domain.Leader

typealias NetLeader = net.appsynth.example.feature.data.source.api.dto.Leader

class LeaderBoardMapper {

    private val languageStatsMapper = LanguageStatsMapper()
    private val userInfoMapper = UserInfoMapper()

    fun map(netLeader: NetLeader): Leader? {
        val rank = netLeader.rank ?: return null
        val languageStats = languageStatsMapper.map(netLeader.runningTotal)
        val userInfo = userInfoMapper.map(netLeader.user) ?: return null
        return Leader(
            rank,
            languageStats,
            userInfo
        )
    }
}