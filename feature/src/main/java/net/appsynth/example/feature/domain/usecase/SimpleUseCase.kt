package net.appsynth.example.feature.domain.usecase

interface SimpleUseCase<T> {
    suspend operator fun invoke(): T
}
