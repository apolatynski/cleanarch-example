package net.appsynth.example.cleanarch.data.source.api

import net.appsynth.example.feature.data.source.api.dto.LeaderBoardResponse
import retrofit2.http.GET

interface WakaTimeApi {

    @GET("api/v1/leaders")
    suspend fun leaderBoard(): LeaderBoardResponse
}