package net.appsynth.example.feature.presentation

data class LeaderItem(
    val rank: Int,
    val dailyAverage: String,
    val name: String,
    val topLanguage: String
)
