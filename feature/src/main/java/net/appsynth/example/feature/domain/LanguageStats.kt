package net.appsynth.example.feature.domain

class LanguageStats(
    val dailyAverageTime: Int = 0,
    usageTimes: List<Pair<String, Int>> = emptyList()
) {

    val stats: Map<String, Usage>

    init {
        val totalUsageSeconds = usageTimes.sumBy { it.second }
        stats = if (totalUsageSeconds > 0) {
            usageTimes.map {
                Pair(
                    it.first,
                    Usage(it.second, it.second.toFloat() / totalUsageSeconds * 100)
                )
            }.toMap()
        } else {
            emptyMap()
        }
    }

    data class Usage(
        val totalSeconds: Int,
        val percentage: Float
    )
}
