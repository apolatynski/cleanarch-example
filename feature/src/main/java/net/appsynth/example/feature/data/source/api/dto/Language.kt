package net.appsynth.example.feature.data.source.api.dto

import com.google.gson.annotations.SerializedName

data class Language(
    @SerializedName("name") val name: String?,
    @SerializedName("total_seconds") val totalSeconds: Float?
)