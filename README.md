# Clean Architecture for Android Application Example

Purpose of this project is to give working example of Clean Architecture approach. A place
to demonstrate specific use cases that could be referred to as a guidelines for implementing
Android apps in AppSynth.

Feel free to provide comments, suggestions, new PRs.

## Clean Architecture Implementation Overview

TODO

## Iterations

### 1. Data Flow

Demonstrates simple repository implementation and how different DTOs are used for passing
data between layers.
